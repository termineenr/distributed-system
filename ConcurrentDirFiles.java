package fileSystem;

import java.time.LocalDateTime;

public class ConcurrentDirFiles {
	
	private LocalDateTime time;
	private int counter = 0;
	
	public ConcurrentDirFiles(LocalDateTime t) 
	{
		// TODO Auto-generated constructor stub
		counter++;
		time = t;
	}

	public LocalDateTime getTime()
	{
		return time;
	}
	
	public int getCounter()
	{
		return counter;
	}
	
	public void setTime(LocalDateTime t)
	{
		time = t;
	}
	
	public void setCounter(int c)
	{
		counter = c;
	}
	public String toString()
	{
		return "time: "+time+" counter: "+counter;
	}
	public boolean equals(ConcurrentDirFiles cdf)
	{
		if(this.counter == cdf.getCounter() && this.time.compareTo(cdf.getTime()) == 0)
			return true;
		return false;
	}
	public int compareTo(ConcurrentDirFiles cdf)
	{
		return (this.time.compareTo(cdf.getTime()));
			
	}
}

package fileSystem;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class DataServer {
	private static AtomicInteger key = new AtomicInteger();
	private HashMap<Integer, SynchronizedPath> fileLists = new HashMap<Integer, SynchronizedPath>();
	private HashSet<Address> serverDirList = new HashSet<>();
	private ReentrantReadWriteLock rwLockSDL = new ReentrantReadWriteLock();
	private Lock rLockSDL = rwLockSDL.readLock();
	private Lock wLockSDL = rwLockSDL.writeLock();
	private String storageLocation;
	private static Address myPos = null;
	private static ServerSocket server = null;
	
	public DataServer(String ip, int port) {
		myPos = new Address(ip, port);
	}
	public static void main(String[] args) {
		DataServer ds = new DataServer(args[0], Integer.valueOf(args[1]));
		try
		{
			server = new ServerSocket(myPos.getPort());
			ds.storageLocation = "DataServer_" + myPos.getIp() + myPos.getPort();
			try{
				Files.createDirectory(Paths.get(ds.storageLocation));
			}
			catch(Exception e)
			{
				System.out.println("Directory already exist");
			}
			System.out.println("SI");
			ds.getSDPosition();
			ds.register();
			
			// ciclo infinito, in attesa di connessioni
			while(true)
			{
				// chiamata bloccante, in attesa di una nuova connessione
				Socket dirCall = server.accept();

				// la nuova richiesta viene gestita da un thread indipendente, si ripete il ciclo
				Thread newServant = new Thread(ds.new DataServant(dirCall));
				newServant.start();
			}
		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}
	
	public boolean getSDPosition() {
		Socket s = null;
		ObjectInputStream ois = null;
		ObjectOutputStream oos = null;
		
		try {
			s = new Socket("localhost", 1234);
			oos = new ObjectOutputStream(s.getOutputStream());
			ois = new ObjectInputStream(s.getInputStream());
			while(true) {
				
				String line = "DataServer";
				oos.writeObject(line);
				oos.flush();
				
				oos.writeObject(myPos);
				oos.flush();
				
				synchronized(serverDirList) {
					serverDirList = (HashSet<Address>) ois.readObject();
					if(serverDirList == null) {
						System.out.println("ciclo");
						continue;
					}
					System.out.println("ciclo");
				}
				oos.close();
				ois.close();
				s.close();
				return true;
			
			}
		}
		catch(IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void register() {
		Iterator<Address> serverDirIter;
		synchronized(serverDirList) {
			serverDirIter = serverDirList.iterator();
		}
		while(serverDirIter.hasNext()){
			Thread newServant = new Thread(new RegServant(serverDirIter.next()));
			newServant.start();
		}
	}

	class RegServant implements Runnable{
		private Address serverDPos = null;
		
		public RegServant(Address serverDPos) {
			this.serverDPos = serverDPos;
		}
		
		@Override
		public void run() {
			Socket s = null;
			ObjectOutputStream oos = null;
			ObjectInputStream ois = null;

			try {
				System.out.println("Registered into <" + serverDPos.getIp() + ":" + serverDPos.getPort() + ">");
				s = new Socket(serverDPos.getIp(), serverDPos.getPort());
				oos = new ObjectOutputStream(s.getOutputStream());
				ois = new ObjectInputStream(s.getInputStream());
				while(true) {
					String line = "RegisterDS";
					oos.writeObject(line);
					oos.flush();
					
					oos.writeObject(myPos);
					oos.flush();
					boolean success = (boolean) ois.readObject();
					
					oos.close();
					ois.close();
					s.close();
					return;
				
				}
			}
			catch(IOException | ClassNotFoundException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
	class DataServant implements DataServerInterface, Runnable {

		private Socket s = null;
		private ObjectInputStream ois = null;
		private ObjectOutputStream oos = null;
		private HashSet<Address> mySDList = new HashSet<>();
		
		public DataServant(Socket s) {
			this.s = s;
			synchronized(serverDirList) {
			mySDList.addAll(serverDirList);
			}
		}
		
		@Override
		public void run() {
			try
			{
				// inizializza i buffer in entrata e uscita
				oos = new ObjectOutputStream(s.getOutputStream());
				ois = new ObjectInputStream(s.getInputStream());
				
				//TO DO varie operazioni
				String line = (String)ois.readObject();
				System.out.println(Thread.currentThread().getName() + " active and running <" + line + ">\n");
				//le varie chiamate
				if(line.equals("createFile")) {
					
					String dir = (String)ois.readObject();
					String name = (String)ois.readObject();					
			        //legge i byte in arrivo
			        byte [] buffer = (byte[])ois.readObject();
			        System.out.println(Thread.currentThread().getName() + " has received the file <" + name + ">");
			        Address SDpos = (Address)ois.readObject();//secondo dataserver
			        //crea il file
			        Integer fileID = this.createFile(dir, name, buffer, SDpos);
			        boolean success = false;
			        if(fileID != null){
			        	success = true;
			        	System.out.println(Thread.currentThread().getName() + " has stored the file <" + name + ">\n");
			        }
			        oos.writeObject(success);
			        oos.flush();
			        	
				}
				
				else if(line.equals("justCreate")) {
					byte [] buffer = (byte[])ois.readObject();
					System.out.println(Thread.currentThread().getName() + " has received the file");
					Integer fileID = this.createFile(null, null, buffer, null);
					System.out.println(Thread.currentThread().getName() + " has stored the file\n");
					oos.writeObject(fileID);
			        oos.flush();
				}
				
				else if(line.equals("deleteFile")) {
					
					Integer fileID = (Integer)ois.readObject();
					System.out.println(Thread.currentThread().getName() + " is deleting the file with ID <" + fileID + ">");
					boolean success = this.deleteFile(fileID);
					System.out.println(Thread.currentThread().getName() + " has deleted the file with ID <" + fileID + ">\n");
					oos.writeObject(success);
		        	oos.flush();
				}
				else if(line.equals("readFile")) {
					
					Integer fileID = (Integer)ois.readObject();
					byte[] buffer = this.readFile(fileID);
					oos.writeObject(buffer);
					oos.flush();
					
				}
				else if(line.equals("uploadFile")) {
					
					Integer fileID = (Integer)ois.readObject();
			        byte [] buffer = (byte[])ois.readObject();
			        
			        System.out.println(Thread.currentThread().getName() + " has uploaded the file with ID <" + fileID + ">\n");
			        boolean success = uploadFile(fileID, buffer);
			        oos.writeObject(success);
			        oos.flush();
			        
				}
				else if(line.equals("subscribe")) {
					
					System.out.println("subscribe");
					Address dirServer = (Address)ois.readObject();
					System.out.println("Ciaone");
					boolean success = this.subscribe(dirServer);
					System.out.println(Thread.currentThread().getName() + " Server Directory"+ dirServer.getIp()+":"+dirServer.getPort()+" added");
					oos.writeObject(success);
					oos.flush();
				}
				
				oos.close();
				ois.close();
				s.close();

			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		}

		
		@Override
		public Integer createFile(String dir, String name, byte[] buffer, Address SDpos) {
			Integer fileID = null;
			try {
				fileID = (Integer) key.getAndIncrement();
				Path path = Paths.get(storageLocation + "/" + fileID);
		        Files.write(path, buffer, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
		        
		        if(dir == null) {
		        	synchronized(fileLists) {
			        	fileLists.put(fileID, new SynchronizedPath(path));
			        }
		        	return fileID;
		        }
				//invio creazione ad un secondo DataServer
				Integer theOtherKey = null;
				Socket theOtherSD;
				ObjectOutputStream oos2;
				ObjectInputStream ois2;
				String method;
				DSFileID me;
				
				if(SDpos != null){
					theOtherSD = new Socket(SDpos.getIp(), SDpos.getPort());
					oos2 = new ObjectOutputStream(theOtherSD.getOutputStream());
					ois2 = new ObjectInputStream(theOtherSD.getInputStream());
					method = "justCreate";
					oos2.writeObject(method);
					oos2.flush();
					oos2.writeObject(buffer);
					oos2.flush();
					theOtherKey = (Integer)ois2.readObject();
					theOtherSD.close();
					oos2.close();
					ois2.close();		        	
					me = new DSFileID(myPos, fileID, SDpos, theOtherKey);
				}
				else{
					me = new DSFileID(myPos, fileID);
				}
				System.out.println("quo");
		        
		        
		        //cerco di creare l'associazione su due serverDirectory
		        ObjectOutputStream oos3;
		        ObjectInputStream ois3;
		        int success = 0;
		        while(!mySDList.isEmpty() && success < 2) {
		        	Address pos = this.chooseSD();
			        //si connette al serverDirectory su cui creare l'associazione
			        
			        Socket servDirSock = new Socket(pos.getIp(), pos.getPort());
			        oos3 = new ObjectOutputStream(servDirSock.getOutputStream());
					ois3 = new ObjectInputStream(servDirSock.getInputStream());
					
					//invia la richiesta
					String line = "createAssociation";
					oos3.writeObject(line);
					oos3.flush();
									
					oos3.writeObject(dir);
					oos3.flush();
					oos3.writeObject(name);
					oos3.flush();
					oos3.writeObject(me);
					oos3.flush();
					boolean isCreated =(boolean)ois3.readObject();
					if(isCreated) {
						success++;
					}
					oos3.close();
					ois3.close();
					servDirSock.close();		        	
		        }
		        //se non riesco a creare l'associazione su almeno un serverDirectory elimino i file dai DataServer
				if(success < 1) {
					Files.delete(path);//se per qualche motivo non si riesce a creare l'associazione elimino il file
					fileID = null;
					if(SDpos != null){
						theOtherSD = new Socket(SDpos.getIp(), SDpos.getPort());
						oos2 = new ObjectOutputStream(theOtherSD.getOutputStream());
						ois2 = new ObjectInputStream(theOtherSD.getInputStream());
						method = "deleteFile";
						oos2.writeObject(theOtherKey);
						oos2.flush();
						boolean ok = (boolean)ois2.readObject();
						theOtherSD.close();
						oos2.close();
						ois2.close();
					}
					
				}
				else {
					synchronized(fileLists) {
			        	fileLists.put(fileID, new SynchronizedPath(path));
			        }
				}
				return fileID;
			}
			catch(IOException | ClassNotFoundException ioe) {
				ioe.printStackTrace();
			}        
			return fileID;
		}
		
		private Address chooseSD() {
			Iterator<Address> tmp = mySDList.iterator();
			int tmpPos = ((int)(Math.random() * (mySDList.size() - 1))); //per ora scelgo a caso su quale serverDir creare l'associazione
			int i = 0;			
			while(i!=tmpPos){
				tmp.next();
				i++;
			}
			Address pos = tmp.next();
			mySDList.remove(pos);
			return pos;
		}
		
		@Override
		public boolean deleteFile(Integer fileID) {
			SynchronizedPath path;
			boolean success = false;
			synchronized (fileLists) {
				path = fileLists.get(fileID);
				path.writeLock();
			}
			try {
		        success = Files.deleteIfExists(path.getPath());
		        path.writeUnlock();
		        if(success) {
			        synchronized(fileLists) {
			        	fileLists.remove(fileID);
			        }
		        }
			}
	        catch(IOException ioe) {
	        	ioe.printStackTrace();
	        }
			
			return success;
		}

		@Override
		public byte[] readFile(Integer fileID) {
			SynchronizedPath path;
			synchronized (fileLists) {
				path = fileLists.get(fileID);
				path.readLock();
			}
			byte[] buffer = null;
			try {	
				buffer = Files.readAllBytes(path.getPath());
			} 
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
			path.readUnlock();
			return buffer;
		}

		@Override
		public boolean uploadFile(Integer fileID, byte[] buffer) {
			SynchronizedPath path;
			boolean success = false;
			synchronized (fileLists) {
				path = fileLists.get(fileID);
				path.writeLock();
			}
			
			try {
		        Path newPath = Files.write(path.getPath(), buffer, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
		        if(path.equals(newPath))
		        	success = true;
			}
	        catch(IOException ioe) {
	        	ioe.printStackTrace();
	        }
			path.writeUnlock();
			return success;
		}
		
		public boolean subscribe(Address dirServer) {
			boolean success = false;
			wLockSDL.lock();
			{
				success = serverDirList.add(dirServer);
			}
			wLockSDL.unlock();
			return success;
		}
	}
	
	class SynchronizedPath{
		
		private ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
		private Lock rLock = rwLock.readLock();
		private Lock wLock = rwLock.writeLock();
		private Path  path = null;
		
		public SynchronizedPath(Path path) {
			this.path = path;
		}
		
		public Path getPath() {
			return path;
		}
		
		public void readLock() {
			rLock.lock();
			return;
		}
		public void readUnlock() {
			rLock.unlock();
			return;
		}
		
		public void writeLock() {
			wLock.lock();
			return;
		}
		
		public void writeUnlock() {
			wLock.unlock();
			return;
		}
	}

}

package fileSystem;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.Scanner;

public class Client2 {
	
	static Socket serverDir = null;
	static Socket serverNaming = null;
	static Address serverNamingAddr = null;
	static Address serverDirAddr = null;
	static String myWorkSpace  = null;
	
	public static void main(String[] args) {
		myWorkSpace = args[0];
		try{
			Files.createDirectory(Paths.get(myWorkSpace));
		}
		catch(Exception e)
		{
			System.out.println("Directory already exist");
		}
		Scanner sc = new Scanner(System.in);
		serverNamingAddr = new Address("localhost", 1234);
		Address server = null;
		boolean exit = false;
		String command = "help";
		System.out.println("Client Start");
		while(!exit) {
			if(command.equals("help")) 
			{
				System.out.println("Chose: ");
				System.out.println("	Connect : connect");
				System.out.println("	Read File : read");
				System.out.println("	Look Directory Files: ls");
				System.out.println("	Make file : mkfile");
				System.out.println("	Make Directory: mkdir");
				System.out.println("	Upload File : upfile");
				System.out.println("	Rename File : rnfile");
				System.out.println("	Rename Directory : rndir");
				System.out.println("	Delete File : rmfile");
				System.out.println("	Delete Directory : rmdir");
				System.out.println("	Help : help");
				System.out.println("	Exit : quit");
			}
			command = sc.next();
			if(command.equals("connect"))
			{
				server = connect();
				
				if(server != null)
				{					
					System.out.println("Established Connection");
				}
				else
				{
					System.out.println("Connection errror");
				}
				System.out.println("Server : "+server.getIp()+":"+server.getPort());
			}
			else if(server != null)
			{
				
				if(command.equals("read"))
				{
					String dir = null;
					String name = null;
					System.out.println("		In which directory");
					dir = sc.next();
					System.out.println("		Which file");
					name = sc.next();
					readFile(dir, name);
				}
				else if(command.equals("ls"))
				{
					HashSet<String> ls = new HashSet<String>();
					String dir = null;
					System.out.println("		Which directory");
					dir = sc.next();
					ls = lookDirFiles(dir);
					System.out.println("The directory "+ dir + " contains:\n"+ls);
				}
				else if(command.equals("mkfile"))
				{
					byte[] buff = null;
					Path path = null;
					String dir = null;
					String name = null;
					String file = null;
				
					System.out.println("In which directory");
					dir = sc.next();
					System.out.println("Which file");
					file = sc.next();
					Scanner scan = new Scanner(file).useDelimiter("\\\\");
					while(scan.hasNext()){
						name = scan.next();
					}
					path = Paths.get(file);
			        try{
			        	buff = Files.readAllBytes(path);
			 
			        }
			        catch(Exception e ) {
			        	e.printStackTrace();
			        }
			        if(createFile(dir, name, buff))
			        	System.out.println("File created");
			        else
			        	System.out.println("File did not create");
				}
				else if(command.equals("mkdir"))
				{
					String dir = null;
					String name = null;
					System.out.println("In which directory");
					dir = sc.next();
					System.out.println("Which name");
					name = sc.next();
					if(createDir(dir, name))
						System.out.println("Directory created");
					else
			        	System.out.println("File did not create");
				}
				else if(command.equals("upfile"))
				{
					byte[] buff = null;
					Path path = null;
					String dir = null;
					String name = null;
					String file = null;
				
					System.out.println("In which directory");
					dir = sc.next();
					System.out.println("Which file");
					file = sc.next();
					Scanner scan = new Scanner(file).useDelimiter("\\\\");
					while(scan.hasNext()){
						name = scan.next();
					}
					path = Paths.get(file);
			        try{
			        	buff = Files.readAllBytes(path);
			 
			        }
			        catch(IOException ioe){
			        	ioe.printStackTrace();
			        }
					
			        if(uploadFile(dir, name, buff))
			        	System.out.println("File uploaded");
			        else
			        	System.out.println("File did not upload");
				}
				else if(command.equals("rnfile"))
				{
					String dir = null;
					String name = null;
					String newName = null;
					System.out.println("In which directory");
					dir = sc.next();
					System.out.println("Which file");
					name = sc.next();
					System.out.println("New file name");
					newName = sc.next();
					if(renameFile(dir, name, newName))
						System.out.println("File renamed");
			        else
			        	System.out.println("File did not rename");
				}
				else if(command.equals("rndir"))
				{
					String dir = null;
					String name = null;
					System.out.println("Which directory");
					dir = sc.next();
					System.out.println("New directory name");
					name = sc.next();
					if(renameDir(dir, name))
						System.out.println("Directory renamed");
			        else
			        	System.out.println("Directory did not rename");
				}
				else if(command.equals("rmfile"))
				{
					String dir = null;
					String name = null;
					System.out.println("Which directory");
					dir = sc.next();
					System.out.println("Which file");
					name = sc.next();
					if(deleteFile(dir, name))
						System.out.println("File deleted");
			        else
			        	System.out.println("File did not delete");
				}
				else if(command.equals("rmdir"))
				{
					String dir = null;
					String name = null;
					System.out.println("		Which directory");
					dir = sc.next();
					System.out.println("		which directory");
					name = sc.next();
					if(deleteDir(dir, name))
						System.out.println("Directory deleted");
			        else
			        	System.out.println("Directory did not delete");
				}
				else
				{
					//System.out.println("Wrong Command");
					command = "help";
				}
					
			}
			else if(command.equals("quit"))
			{
				exit = true;
			}
			else {
				System.out.println("You must connect first");
			}
			command = "help";
		}
		try {
			serverDir.close();
			serverNaming.close();
		}catch(Exception e) {}
		sc.close();
	}

	public static Address connect() {
		Address server = null;
		try {
			serverNaming = new Socket(serverNamingAddr.getIp(), serverNamingAddr.getPort());
			
			ObjectOutputStream oos = new ObjectOutputStream(serverNaming.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(serverNaming.getInputStream());
		
			String request = new String("clientRequest");
			oos.writeObject(request);
			oos.flush();
			server = (Address)ois.readObject();	
			serverDirAddr = server;
			serverDir = new Socket(server.getIp(), server.getPort());
			oos.close();
			ois.close();
		}
		catch(Exception e ){
			server = null;
			e.printStackTrace();
		}
		return server;
	}


	public Address checkServerDirectory(String serverDirName) {
		try {
			serverNaming = new Socket(serverNamingAddr.getIp(), serverNaming.getPort());
			
			ObjectOutputStream oos = new ObjectOutputStream(serverNaming.getOutputStream());
					
			String request = new String("checkActiveServerDir");
			oos.writeObject(request);
			oos.writeObject(serverDirName);
			oos.flush();
			oos.close();
		}
		catch(Exception e ){
			e.printStackTrace();
		}
		return connect();
	}

	public static Object readFile(String dir, String name) {
		byte[] buff = null;
		Path path = null;
		boolean succes = false;
		try {
			serverDir = new Socket(serverDirAddr.getIp(), serverDirAddr.getPort());
			ObjectOutputStream oos = new ObjectOutputStream(serverDir.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(serverDir.getInputStream());
		
			String request = new String("readFile");
			oos.writeObject(request);
			oos.writeObject(dir);
			oos.writeObject(name);
			oos.flush();
			buff = (byte[])ois.readObject();
			path = Paths.get( myWorkSpace + "\\" + name);
	        Files.write(path, buff, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
	        oos.close();
			ois.close();
		}
		catch(Exception e ){
			succes = false;
			e.printStackTrace();
		}
		return succes;
	}

	public static HashSet<String> lookDirFiles(String dir) {
		HashSet<String> ls = new HashSet<String>();
	try {
			serverDir = new Socket(serverDirAddr.getIp(), serverDirAddr.getPort());
			ObjectOutputStream oos = new ObjectOutputStream(serverDir.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(serverDir.getInputStream());
		
			String request = new String("lookDirFiles");
			oos.writeObject(request);
			oos.writeObject(dir);
			oos.flush();
			ls = (HashSet<String>)ois.readObject();
			oos.close();
			ois.close();
		}
		catch(Exception e ){
			e.printStackTrace();
		}
		return ls;
	}

	public static boolean createFile(String dir, String name, Object file) {
		boolean succes = false;
		try {
			serverDir = new Socket(serverDirAddr.getIp(), serverDirAddr.getPort());
			ObjectOutputStream oos = new ObjectOutputStream(serverDir.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(serverDir.getInputStream());
		
			String request = new String("createFile");
			oos.writeObject(request);
			oos.writeObject(dir);
			oos.writeObject(name);
			oos.writeObject(file);
			oos.flush();
			succes = (boolean)ois.readObject();
			oos.close();
			ois.close();
			
		}
		catch(Exception e ){
			succes = false;
			e.printStackTrace();
		}
		return succes;

	}

	public static boolean createDir(String dir, String name) {
		boolean succes = false;
		try {
			serverDir = new Socket(serverDirAddr.getIp(), serverDirAddr.getPort());
			ObjectOutputStream oos = new ObjectOutputStream(serverDir.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(serverDir.getInputStream());
		
			String request = new String("createDir");
			oos.writeObject(request);
			oos.writeObject(dir);
			oos.writeObject(name);
			oos.flush();
			succes = (boolean)ois.readObject();
			oos.close();
			ois.close();
			
		}
		catch(Exception e ){
			succes = false;
			e.printStackTrace();
		}
		return succes;
	}

	public static boolean uploadFile(String dir, String name, byte[] buff) {
		boolean succes = false;
        try{
        	serverDir = new Socket(serverDirAddr.getIp(), serverDirAddr.getPort());
        	ObjectOutputStream oos = new ObjectOutputStream(serverDir.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(serverDir.getInputStream());
		
			String request = new String("uploadFile");
			oos.writeObject(request);
			oos.writeObject(dir);
			oos.writeObject(name);
			oos.writeObject(buff);
			oos.flush();
			succes = (boolean)ois.readObject();
			oos.close();
			ois.close();
        }
        catch(Exception e ) {
        	succes = false;
        	e.printStackTrace();
        }
		return succes;
	}

	public static boolean renameFile(String dir, String name, String newName) {
		boolean succes = false;
        try{
        	serverDir = new Socket(serverDirAddr.getIp(), serverDirAddr.getPort());
        	ObjectOutputStream oos = new ObjectOutputStream(serverDir.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(serverDir.getInputStream());
		
			String request = new String("renameFile");
			oos.writeObject(request);
			oos.writeObject(dir);
			oos.writeObject(name);
			oos.writeObject(newName);
			oos.flush();
			succes = (boolean)ois.readObject();
			oos.close();
			ois.close();
 
        }
        catch(Exception e ) {
        	succes = false;
        	e.printStackTrace();
        }
		return succes;
	}

	public static boolean renameDir(String dir, String name) {
		boolean succes = false;
        try{
        	serverDir = new Socket(serverDirAddr.getIp(), serverDirAddr.getPort());
        	ObjectOutputStream oos = new ObjectOutputStream(serverDir.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(serverDir.getInputStream());
		
			String request = new String("renameDir");
			oos.writeObject(request);
			oos.writeObject(dir);
			oos.writeObject(name);
			oos.flush();
			succes = (boolean)ois.readObject();
			oos.close();
			ois.close();
        }
        catch(Exception e ) {
        	succes = false;
        	e.printStackTrace();
        }
		return succes;
	}

	public static boolean deleteFile(String dir, String name) {
		boolean succes = false;
        try{
        	serverDir = new Socket(serverDirAddr.getIp(), serverDirAddr.getPort());
        	ObjectOutputStream oos = new ObjectOutputStream(serverDir.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(serverDir.getInputStream());
		
			String request = new String("deleteFile");
			oos.writeObject(request);
			oos.writeObject(dir);
			oos.writeObject(name);
			oos.flush();
			succes = (boolean)ois.readObject();
			oos.close();
			ois.close();
        }
        catch(Exception e ) {
        	succes = false;
        	e.printStackTrace();
        }
		return succes;
	}

	public static boolean deleteDir(String dir, String name) {
		boolean succes = false;
        try{
        	serverDir = new Socket(serverDirAddr.getIp(), serverDirAddr.getPort());
        	ObjectOutputStream oos = new ObjectOutputStream(serverDir.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(serverDir.getInputStream());
		
			String request = new String("deleteDir");
			oos.writeObject(request);
			oos.writeObject(dir);
			oos.writeObject(name);
			oos.flush();
			succes = (boolean)ois.readObject();
			oos.close();
			ois.close();
        }
        catch(Exception e ) {
        	succes = false;
        	e.printStackTrace();
        }
		return succes;
	}


}
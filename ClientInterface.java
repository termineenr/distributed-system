package fileSystem;

import java.util.HashSet;

public interface ClientInterface 
{
	public String connect();
	public String checkServerDirectory(String serverDirName);
	public Object readFile(String dir, String name);
	public HashSet<String> lookDirFiles(String dir);
	public boolean createFile(String dir, String name, Object file);
	public boolean createDir(String dir, String name);
	public boolean uploadFile(String dir, String name, byte[] buff);
	public boolean renameFile(String dir, String name);
	public boolean renameDir(String dir, String name);
	public boolean deleteFile(String dir, String name);
	public boolean deleteDir(String dir, String name);
}

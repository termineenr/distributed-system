NOTE: To try the functioning of our system you need at least four terminal windows.

First run the NAMING SERVER:
    java fileSystem.ServerNaming

Then run at least one DIRECTORY SERVER and one DATA SERVER:
    java fileSystem.ServerDirectory localhost <portNumber>
    java fileSystem.DataServer localhost <portNumber>
If you want you can run as many DIRECTORY SERVER/DATA SERVER as you want.
The DATA SERVER will create a directory named DataServer_localhost<portNumber> that simulates the phisical memory of 
the DATA SERVER (you will find the file stored in that DATA SERVER).

Finally run at least one CLIENT:
    java fileSystem.Client2 <name>
A directory named <name> will be created: here you will find the files obtained with the read command.

Once you start the CLIENT you will see the following list of available commands:

    connect: will connect to the system;
    read <directoryPath> <fileName>: will download <fileName> contained in <directoryPath> in the CLIENT directory; 
    ls <directoryPath>: will return the list of files and directories contained in <directoryPath>
    mkfile <directoryPath> <filePath\fileName>: will upload the file named <fileName> contained in <filePath> in the <directoryPath> of the system
    mkdir <parentDir> <dirName>: will create a new directory named <dirName> in <parentDir>, the "/" is the root directory;
    upfile <directoryPath> <filePath\fileName>: will upgrade the file named <fileName> contained in <filePath> in the <directoryPath> of the system
    rnfile <directoryPath> <fileName> <newFileName>: will rename <fileName> contained in <directoryPath> with the new name <newFileName>;
    rndir <directoryPath> <newDirName>: will rename the last directory in <directoryPath> with the new name <newDirName>;
    rmfile <directoryPath> <fileName>: will delete the file <fileName> contained in <directoryPath>;
    rmdir <directoryPath> <dirName>: will delete the directory <dirName> contained in <directoryPath>;

As first, you have to run the command "connect".

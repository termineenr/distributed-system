package fileSystem;

import java.util.*;
import java.io.*;


public class DirectoryTreeExample
{
	public static void main(String[] args)
	{
		DirectoryTree tree = new DirectoryTree();
		List<String> ls = new ArrayList<String>();
		tree.addDirectory("/A/a");
		tree.addDirectory("/A/b");
		tree.addDirectory("/A/c");
		tree.addDirectory("/B/l");
		tree.addDirectory("/C/m");
		tree.addDirectory("/D/n");

		System.out.println("Element of /A :");
		ls = tree.getDirectory("/A");
		for(int i = 0; i < ls.size() ; i++)
			System.out.println(ls.get(i));

		System.out.println("1 Contiene : " + tree.contains("/C/m"));
		System.out.println("Element of /C :");
		
		ls = tree.getDirectory("/C");
		for(int i = 0; i < ls.size() ; i++)
			System.out.println(ls.get(i));

		tree.renameDirectory("/C/m", "k");
		System.out.println("2 Contiene :" + tree.contains("/C/k"));
		
		System.out.println("Element of /C :");
		ls = tree.getDirectory("/C");
		for(int i = 0; i < ls.size() ; i++)
			System.out.println(ls.get(i));

		tree.removeDirectory("/C/k");
		System.out.println("3 Contiene :" + tree.contains("/C/k"));
		
		System.out.println("Element of /C :");
		ls = tree.getDirectory("/C");
		for(int i = 0; i < ls.size() ; i++)
			System.out.println(ls.get(i));

		System.out.println("Element of / :");
		ls = tree.getDirectory("/");
		for(int i = 0; i < ls.size() ; i++)
			System.out.println(ls.get(i));
	}
}
